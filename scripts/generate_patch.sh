#!/bin/bash

#SPDX-License-Identifier: BSD-3-Clause
#Copyright (c) 2022 hammoz

#------------------------------------------------------------------------------------------------------
# Sylvaine Ferrachat 2022-01
#
# Script to automatically generate a correct patch on top of the official icon version as recorded
# by the super project submodule SHA.
# Note that the produced patch, for being later used, should ultimately replace the distributed 
# `<icon-ham_distribution>/patch_for_icon.diff` file.
#
# Usage:
# ------
# path/to/generate_patch.sh <some output file, e.g. patch.diff>
#
#------------------------------------------------------------------------------------------------------

set -e

#-- Initializations
this_script="$0"
this_script=`readlink -e $this_script`
script_dir=`dirname $this_script`
rootdir=`dirname $script_dir`
icondir="${rootdir}/icon"
distrib_patch="${rootdir}/patch_for_icon.diff"

#-- Load utilities
source ${script_dir}/lib/utilities.sh

#-- Arguments
patch_file="$1"

if [ $# -ne 1 ] ; then
    echo "Error! '`basename $this_script`' requires exactly one argument (output file name)!"
    exit 1
fi

#----------------------------------------
# Main

#-- Fetch the icon submodule sha as recorded in super project
icon_sha=`get_subm_sha $rootdir icon`

#-- Create the patch
git -C $icondir diff $icon_sha -- ":(exclude)externals/hammoz" > $patch_file

#-- Finish
if [[ "`readlink -e $patch_file`" != "$distrib_patch" ]] ; then
    msg="Successfully created '$patch_file'!\n"
    msg+="You may now check it by comparing it to $distrib_patch,\n"
    msg+="and ultimately:\n"
    msg+="    - rename it to $distrib_patch;\n"
    msg+="    - commit the changes.\n"
else
    msg="Successfully overwrote '$distrib_patch'!\n"
    msg+="You may now check the changes and commit them."
fi
echo -e "$msg"

exit 0
