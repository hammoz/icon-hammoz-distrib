#!/bin/bash

#SPDX-License-Identifier: BSD-3-Clause
#Copyright (c) 2021 hammoz

#------------------------------------------------------------------------------------------------------
# Sylvaine Ferrachat 2022-01
#
# Utility functions for the `scripts/XX.sh` main scripts
#
#------------------------------------------------------------------------------------------------------

#-----------------------------------------
#-- Function to ask a yes-no question interactively to the user
#
# It assumes a correspondance between yes and true, and no and false

function interactive_yes_no()
{

  local question_text="$1"
  local default_answer="$2" # true (yes) or false (no)

  case $default_answer in
      true|false)
        :
      ;;
      *)
        echo "Wrong format for 2nd argument of function 'interactive_yes_no'" >&2
        exit 1
      ;;
  esac

  if $default_answer ; then
     message="${question_text} (y/n [y])"
  else
     message="${question_text} (y/n [n])"
  fi

  while true ; do
      read -p "$message " ans

      ans=`echo $ans | tr '[:upper:]' '[:lower:]'`

      case $ans in
           'y'|'yes')
              echo "true"
              return
           ;;
           'n'|'no')
              echo "false"
              return
           ;;
           '')
              echo "$default_answer"
              return
           ;;
           *)
              echo "Please answer 'y','yes','n','no' (case insensitive) or leave empty" >&2
           ;;
      esac
  done

}

#----------------------------------------
#-- Function to fetch any submodule sha
#   as recorded in super project

function get_subm_sha()
{
    local super_proj_dir="$1"
    local subm_name="$2"

    git -C $super_proj_dir submodule status --cached $subm_name | sed -e 's/^.\([^ ]*\) .*$/\1/'
}
