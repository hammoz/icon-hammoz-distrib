Utility scripts
================

Sylvaine Ferrachat, ETH Zurich, 2022-01

#------------------------------------------------------
## CONTENTS

```
 * `deploy.sh`         # <-- Script to deploy the whole icon-ham infrastructure

 * `restore_icon.sh`   # <-- Script to restore the `icon/` subdir in a pristine way
                       #     w.r.t. the official icon distribution as recorded by
                       # the `icon` git submodule

 * `generate_patch.sh` # <-- Script to automatically generate a correct patch on top
                       #     of the official icon version

 * `lib/`              # <-- Stores utility functions

For more details, please inspect the header of each script.
