#!/bin/bash

#SPDX-License-Identifier: BSD-3-Clause
#Copyright (c) 2021 hammoz


#------------------------------------------------------------------------------------------------------
# Sylvaine Ferrachat 2021-07
#
# Script to deploy the whole icon-ham infrastructure, based on the following 3 ingredients:
# 1. A pristine icon tag (requires access to the official icon repository)
# 2. Hammoz
# 3. A patch that introduces some necessary icon modifications so that it make calls to hammoz
#
# Usage:
# ------
# path/to/deploy.sh
#
#------------------------------------------------------------------------------------------------------

set -e

#-- Initializations
this_script="$0"
this_script=`readlink -e $this_script`
script_dir=`dirname $this_script`
rootdir=`dirname $script_dir`
patch="${rootdir}/patch_for_icon.diff"
ancill="${rootdir}/ancillaries"
list_new_files="${ancill}/list_of_added_files_to_icon.txt"

cd "$rootdir"

#-- Init submodules and checkout them
#   /!\ No recursive checkout here. It won't work
#   because icon's original submodule have
#   obsolete URLs that no longer exist
echo "Initializing hammoz and icon..."
echo "-------------------------------"
git submodule update --init
echo


cd ${rootdir}/icon

#-- Apply patch to icon
echo "Applying patch to icon..."
echo "-------------------------"
git apply --whitespace=nowarn "$patch"
echo

#-- Initialize and download the icon submodules
#   /!\ Now possible because their URL has been
#   corrected by the patch
echo "Initializing the icon submodules..."
echo "-----------------------------------"
git submodule sync --recursive # necessary to update URLs in icon/
git submodule update --init --recursive
echo

#-- Link hammoz into icon externals
echo "Linking hammoz into the icon externals..."
echo "-----------------------------------------"
ln -sf ../../hammoz externals/.
echo

#-- Make git aware of the newly added files
echo "Include the newly introduced files to git tracking..."
echo "-----------------------------------------------------"
sed -e '/^#/d' $list_new_files | xargs git add -N
echo

#-- Finish
echo "Done! You may now change directory to 'icon/' and build the model."
echo

exit 0
