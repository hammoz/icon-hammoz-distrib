#!/bin/bash

#SPDX-License-Identifier: BSD-3-Clause
#Copyright (c) 2022 hammoz

#------------------------------------------------------------------------------------------------------
# Sylvaine Ferrachat 2022-01
#
# Script to restore the `icon/` subdir in a pristine way w.r.t. the official distribution of the relevant tag
# as recorded by the `icon` git submodule.
#
# Usage:
# ------
# path/to/restore_icon.sh
#
#------------------------------------------------------------------------------------------------------

set -e

#-- Initializations
this_script="$0"
this_script=`readlink -e $this_script`
script_dir=`dirname $this_script`
rootdir=`dirname $script_dir`
icondir="${rootdir}/icon"

#-- Load utilities
source ${script_dir}/lib/utilities.sh

#----------------------------------------
# Main

cd $icondir

#-- Fetch the icon submodule sha as recorded in super project
icon_sha=`get_subm_sha $rootdir icon`

#-- Security
msg="WARNING! By switching to:\n\n"
msg+="`git log -1 --pretty=format:"%h %an %ai %D" $icon_sha`,\n\n"
msg+="this script will discard any uncommitted modification with respect to:\n\n"
msg+="`git log -1 --pretty=format:"%h %an %ai %D"`\n\n"
msg+="and will also remove any untracked file.\n\n"
msg+="If you need to apply again the changes necessary for hammoz,\n"
msg+="you simply need to run the 'deploy.sh' script again.\n\n"

echo -e "$msg"
flag_continue=$(interactive_yes_no \
                "Are you sure you want to continue?" true)

if ! $flag_continue ; then
    exit 0
fi

#-- Proceed with restoration
git switch -d --discard-changes $icon_sha
git clean -f

exit 0
