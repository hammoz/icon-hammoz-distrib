README file for ICON-HAMMOZ-DISTRIB
===================================

Sylvaine Ferrachat, ETH Zurich, 2021-07

#------------------------------------------------------
## IMPORTANT PRELIMINARY NOTICE

This repository is set as convenience to help deploying the whole icon-ham infrastructure,
based on the following 3 ingredients:

1. A pristine icon tag (requires access to the official icon repository)
2. Hammoz
3. A patch that introduces some necessary icon modifications so that it make calls to hammoz

Both the icon code and the hammoz codes are set as git submodules here, meaning that they are
not part of this repository, but will be automatically downloaded upon installation.
Please note that downloading icon requires access to its repository.

This repository is not meant to bear the history of any further development of the patched
icon code.

#------------------------------------------------------
## CONTENTS

```
 * hammoz/ # <-- HAMMOZ code and utilities
 * icon/   # <-- Initially: pristine ICON only
           #     After deployment: ICON + patch + HAMMOZ 
 * patch_for_icon.diff
 * README.md
 * scripts/
``` 
#------------------------------------------------------
## INSTALLATION

### 0. Preliminary step:

Please check the version of git on your system. It is necessary to have a git version >= 2.10 
in order to be able to fully use the capabilities of the scripts.

*[Only necessary if the command `readlink -e .` raises an error on your machine.]*

If you're on a Mac OS or any other unix distribution based on BSD, you may not have
access to the GNU flavor of some basic shell commands. The below `deploy.sh` script
uses the GNU-flavored `readlink` utility.
To check if you're affected by this problem, you may issue:
```
readlink -e .
```
If the command returns an error instead of the full pathname of your current directory,
you need to take action to fix this.

  * On Mac OS, the easiest is to install the GNU coreutils package.
With `homebrew`:
```
brew install coreutils
```
It installs the GNU commands, prefixed with a 'g'. To then use them with their standard
name (as implied by the `deploy.sh` script), add the following in your `~/.bashrc`:
```
PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
```
--> See `brew info coreutils` for details.

If you don't have homebrew on your Mac you may install it (https://brew.sh), or
alternatively you may use the Macports (https://www.macports.org, and use the macport-way
of installing the GNU coreutils).

  * For other cases than Mac OS, please contact your system administrator to have access
to the GNU core utilities on your machine.

### 1. Clone this repository:

```
git clone [URL of the current repository]
```

**Important remark:** *Do not use option* `--recurse-submodules`, as it would fail and
leave your clone in an improper state. The submodule initialization and update will be
done at stage 2..

### 2. Deploy the code:

From the root of your clone, deploy the code with:
```
./scripts/deploy.sh
```

The script will apply the patch, initialize and update the submodules (recusrsively),
and will add a symlink into `icon/externals` for hammoz.

### 3. Build icon:

Move into `icon/` and configure your code as usual:
```
cd icon
./configure --with-fortran=[some fortran compiler] --with-hammoz [further options]
```
The latter flag `--with-hammoz` will enable the configuration and later compiling of
the HAMMOZ code parts inside ICON.

Then, compile your code as usual with `make -j [some number]`

### 4. Start a run

The model is in its current version released with three runscript templates pre-configured for

* a standard present-day simulation with ACCMIP emission data (_exp.atm_amip_hammoz_)
* a standard present-day simulation with ACCMIP emission data for anthropogenic and aircraft emissions but GFAS data for biomass burning emissions (_exp.atm_amip_hammoz_GFAS_)
* a pre-industrial simulation representative for 1850 emission conditions (_exp.atm_amip_hammoz_PIA_) (**Caution**: SSTs and sea ice are set for present day).

For each of the runscripts a corresponding emission-matrix input file (_emi_spec*.txt_) is provided.

**Default settings:**

The default spatial resolution is R02B04L47, the preset period to be simulated is 2002-2013.

Output is written every 6 hours, a new output file is opened at the first day of a new month.

**To generate a runscript from the template:**

Change directory to the icon-basedir of your clone of icon-hammoz-distrib.

Then execute
```
./make_runscripts atm_amip_hammoz 
```

The actual runscript is named exp.atm_amip_hammoz.run and you will find it in the directory 'run'
```
cd run
```

When you want to customize the runscript, think of the following:

* Adjust the start and end time of the experiment.
* Adjust the output frequency (this can be set separately for each output stream).

You might also want to

* Change the name of the script.
* Change the name of the experiment.
* When using a different emission data set - build your own emi_spec_<emidatasetname>.txt file and add or delete where necessary the variables in the output_nml for the emi_tm stream.


**Running the script:**

The exact command to submit your job to the batch queueing system of your machine may differ. 

#------------------------------------------------------
## How to update the `patch_for_icon.diff`

If you have done modifications inside `icon/` and need to share them
with the rest of the HAMMOZ community, you need to update the `patch_for_icon.diff` file,
commit it and push your branch.

A script has been made to facilitate this operation:
```
./script/generate_patch.sh <some patch file name>
```
Note that the produced patch (`<some patch file name>`), for being later used, should ultimately replace the distributed
`<icon-ham_distribution>/patch_for_icon.diff` file.
